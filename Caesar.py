#!/usr/bin/python3

import argparse
import os.path


class Caesar:



	def __init__(self):
		pass



	@classmethod
	def Caesar(cls, message, key):
		
		"""
			Decrypt and Encrypt the message with a key ranging from -25 to 25.
			
			:param message: the message to be encrypted or decrypted.
			:param key: the encryption or decryption key.
			:type message: str
			:type key: int
			:return: returns the result.
			:rtype: str
		"""

		lock = 0


		if isinstance(message, str):
			lock += 1


		if isinstance(key, int):
			lock += 1


		if lock == 2:

			alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			answer = ""

			for m in message:

				m = m.upper()

				if m in (" ", "'", "?", "!"):
					answer += m

				if m in alphabet:
					answer += alphabet[(alphabet.find(m) + key) % 26]
			
			return answer



	@classmethod
	def Crack(cls, message):

		"""
			Crack the encryption message to find the key.

			:param message: the encryption message.
			:type message: str
		"""

		if isinstance(message, str):

			for i in range(1, 26):
				print("-"*50, "\n")
				print("Key :", -i, "\n")
				print(Caesar.Caesar(message, -i), "\n")


try:
	parser = argparse.ArgumentParser()

	parser.add_argument("-f", "--file", type=str)
	parser.add_argument("-k", "--key", type=int)
	parser.add_argument("-s", "--string", type=str)
	parser.add_argument("-c", "--crack", action="store_true")

	args = parser.parse_args()

	_file = args.file
	_key = args.key
	_string = args.string 
	_crack = args.crack


	if _file and _key:
		
		if os.path.isfile(_file):

			if _file.lower().endswith((".txt")):
				
				if _key >= 0:
					filename = _file.split(".")[0] + "_encrypt.txt"
				
				if _key < 0:
					filename = _file.split(".")[0] + "_decrypt.txt"

				with open(filename, "w") as erase:
					pass

				with open(_file, "r") as read:
					for line in read:

						with open(filename, "a+") as save:

							if line != "\n":
								save.write(Caesar.Caesar(line, _key) + "\n")
							else:
								save.write("\n")
			else:
				print("Please put the content in a .txt file")
		else:
			print("File not found or the path to the file not found.\n")
			print("Check if you have the reading rights.\n")


	if _string and _key:
		print(Caesar.Caesar(_string, _key))

	
	if _string and _crack:
		Caesar.Crack(_string)

except:
	pass